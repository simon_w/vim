let g:gutentags_define_advanced_commands = 1
set wig+=**/node_modules/**
set wig+=**/vendor/**
set wig+=vendor.js
set wig+=es6.js
set wig+=scripts.js
set wig+=**/.*-babel/**

" Disable Hack
let g:hack#enable = 1

" Ale config
let g:ale_fixers =  {'javascript': ['eslint']}
let g:ale_fix_on_save = 1
let g:ale_completion_enabled = 1

execute pathogen#infect()

set runtimepath+=~/.vim/bundle/LanguageClient-neovim
let g:phan_quick = 0

" Load all of the helptags now, after plugins have been loaded.
" All messages and errors will be ignored.
silent! helptags ALL

filetype indent plugin on

set autoindent
set autoread
set nobackup
set nobomb
set cmdheight=2
set colorcolumn=81
set confirm
set cursorline

set expandtab
set incsearch
set lazyredraw

set laststatus=2
set list
set listchars=tab:\|\ ,trail:_,extends:>,precedes:<,nbsp:.
set mouse=a
set number relativenumber
set ruler

set shiftwidth=2
set tabstop=2
set softtabstop=-1

set ignorecase
set smartcase
set smartindent
set splitbelow
set splitright
set nostartofline

set rulerformat=%30(%=\:b%n%y%m%r%w\ %l:%c%V/%L\ %P%)
set notimeout

set noswapfile
set nowb

set hidden
set hlsearch
set noerrorbells
set novisualbell
set vb t_vb=

set foldmethod=syntax
set foldlevelstart=1

set spell
set spelllang=en_nz

set guifont=B612\ Mono:h11

colorscheme desert

" Change cursor line to not be indent guides colour
autocmd VimEnter * :hi CursorLine term=underline cterm=underline guibg=gray45

" Indent guide
let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_auto_colors = 0
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=gray30 ctermbg=7
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=gray40 ctermbg=8

" Open NERDTree on startup if not opening a single file
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" Close NERDTree if it's the last window
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

" NERDTree
let NERDTreeShowHidden=1
let NERDTreeIgnore=['^\.git$', '\~$', '\.swp$', '\.bak$', '\.orig$']
let NERDTreeSortOrder=['\/$', '^\.', '\.php$', '*']
let NERDTreeMinimalUI=1

" Unite
let g:unite_source_history_yank_enable = 1
let g:unite_redraw_hold_candidates = 50000
call unite#filters#matcher_default#use(['matcher_fuzzy', 'matcher_hide_hidden_files'])
call unite#filters#sorter_default#use(['sorter_selecta'])

call unite#custom#profile('default', 'context', {
\ 'start_insert': 1,
\ 'prompt': '>> ',
\ 'auto_preview': 1
\})

if executable("ag")
  let g:unite_source_rec_async_command = ['ag', '--follow', '--nocolor',
        \ '--nogroup', '-g', '', '--ignore', 'node_modules']
endif

nnoremap <silent> <leader>t :<C-u>Unite file_rec/async:!<cr>
nnoremap <silent> <space>/ :Unite grep:.<cr>
cabbrev ls <c-r>=(getcmdtype() == ':' && getcmdpos()<=3 ? 'Unite buffer' : 'ls')<CR>

" Remove trailing WS on save
autocmd BufWritePre * :call <Sid>StripTrailingWhitespaces()

" F7 to reindent the entire file
map <F7> mzgg=G`z<CR>

" F8 to vertical-split open path under cursor
map <F8> :vertical wincmd f<CR>

" Run esformatter on <leader>es
nnoremap <silent> <leader>es :Esformatter<CR>
vnoremap <silent> <leader>es :EsformatterVisual<CR>

" Only load one local vimrc, outside a sandbox and without asking
let g:localvimrc_count = 1
let g:localvimrc_sandbox = 0
let g:localvimrc_ask = 0
let g:localvimrc_name = [".lvimrc", ".project"]

" List of custom ftplugins, so that localvimrc doesn't override the settings
let g:custom_ftplugins = ['ruby', 'twig', 'yaml']

" Function to remove empty lines after { and before }
function! CleanBraceLines()
  let _s=@/
  let l = line(".")
  let c = col(".")
  %s/{\n\n/{\r/e
  %s/\n\n\( *\)}/\r\1}/e
  let @/=_s
  call cursor(l, c)
endfunction

" strips trailing whitespace at the end of files
function! <SID>StripTrailingWhitespaces()
    " save last search & cursor position
    let _s=@/
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    let @/=_s
    call cursor(l, c)
endfunction

autocmd FileType php LanguageClientStart

" ALE
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)
