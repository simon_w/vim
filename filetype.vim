if exists("did_load_filetypes")
	finish
endif
augroup filetypedetect
	au! BufNewFile,BufRead *.ss setf html
	au! BufNewFile,BufRead *.md setf markdown
augroup END
